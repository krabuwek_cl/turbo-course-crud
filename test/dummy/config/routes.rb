Rails.application.routes.draw do
  get 'articles/index'
  get 'articles/new'
  get 'articles/edit'
  get 'articles/index'
  get 'articles/new'
  get 'articles/create'
  resources :articles

  mount TurboCourse::Engine => "/turbo_course"
end
