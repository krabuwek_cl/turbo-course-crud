require_relative "lib/turbo_course/version"

Gem::Specification.new do |spec|
  spec.name        = "turbo_course"
  spec.version     = TurboCourse::VERSION
  spec.authors     = ["krabuwek_cl"]
  spec.email       = ["maxim.mikhailau@cleverlabs.io"]
  spec.summary     = "Summary of TurboCourse."
  spec.description = "Description of TurboCourse."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.1.3.4"
end
